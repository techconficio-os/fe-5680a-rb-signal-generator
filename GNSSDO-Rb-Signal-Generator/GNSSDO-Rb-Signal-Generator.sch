EESchema Schematic File Version 4
LIBS:GNSSDO-Rb-Signal-Generator-cache
EELAYER 29 0
EELAYER END
$Descr USLegal 14000 8500
encoding utf-8
Sheet 1 8
Title "GNSSDO Rb Signal Generator"
Date "2020-11-25"
Rev "v0.2.0"
Comp "TechConficio Inc."
Comment1 ""
Comment2 ""
Comment3 "License : Creative Commons By 4.0 International"
Comment4 "Author : Steven Knudsen"
$EndDescr
$Sheet
S 950  850  950  900 
U 5FBF1C99
F0 "10 MHz Reference Buffer" 50
F1 "GNSSDO-Rb-Signal-Generator_01.sch" 50
$EndSheet
$Sheet
S 950  2100 950  900 
U 5FBF1D37
F0 "Clock Buffer" 50
F1 "GNSSDO-Rb-Signal-Generator_02.sch" 50
$EndSheet
$Sheet
S 950  3300 950  850 
U 5FBF1DA1
F0 "Clock Generator" 50
F1 "GNSSDO-Rb-Signal-Generator_03.sch" 50
$EndSheet
$Sheet
S 950  4450 950  900 
U 5FBF1DF5
F0 "Power" 50
F1 "GNSSDO-Rb-Signal-Generator_04.sch" 50
$EndSheet
$Sheet
S 950  5650 950  850 
U 5FBF1E64
F0 "Wireless Controller" 50
F1 "GNSSDO-Rb-Signal-Generator_05.sch" 50
$EndSheet
$Sheet
S 3000 850  900  900 
U 5FBF1EBD
F0 "GNSS" 50
F1 "GNSSDO-Rb-Signal-Generator_06.sch" 50
$EndSheet
$Sheet
S 3000 2050 900  900 
U 5FBF1F2C
F0 "Isolators" 50
F1 "GNSSDO-Rb-Signal-Generator_07.sch" 50
$EndSheet
$EndSCHEMATC
