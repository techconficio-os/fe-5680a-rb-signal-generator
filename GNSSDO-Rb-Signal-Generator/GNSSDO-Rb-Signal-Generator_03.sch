EESchema Schematic File Version 4
LIBS:GNSSDO-Rb-Signal-Generator-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 8
Title "GNSSDO Rb Signal Generator"
Date "2020-11-25"
Rev "v0.2.0"
Comp "TechConficio Inc."
Comment1 ""
Comment2 ""
Comment3 "License : Creative Commons By 4.0 International"
Comment4 "Author : Steven Knudsen"
$EndDescr
$EndSCHEMATC
