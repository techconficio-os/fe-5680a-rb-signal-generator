EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr USLegal 14000 8500
encoding utf-8
Sheet 1 9
Title "GNSSDO Rb Signal Generator"
Date "2020-11-23"
Rev "0.2.0"
Comp "TechConficio Inc."
Comment1 ""
Comment2 "License : CC BY 4.0"
Comment3 "Author : Steven Knudsen"
Comment4 "Author : Steven Knudsen"
$EndDescr
$Sheet
S 750  950  800  800 
U 5E7FCA69
F0 "10 MHz Reference Buffer" 60
F1 "GNSSDO-Rb-Signal-Generator-v10_SS_1.sch" 60
$EndSheet
$Sheet
S 750  2150 800  800 
U 5E7FCA6B
F0 "Clock Buffer" 60
F1 "GNSSDO-Rb-Signal-Generator-v10_SS_2.sch" 60
$EndSheet
$Sheet
S 750  3400 800  800 
U 5E7FCA6D
F0 "Clock Generator" 60
F1 "GNSSDO-Rb-Signal-Generator-v10_SS_3.sch" 60
$EndSheet
$Sheet
S 750  4650 800  800 
U 5E7FCA6F
F0 "Power" 60
F1 "GNSSDO-Rb-Signal-Generator-v10_SS_4.sch" 60
$EndSheet
$Sheet
S 750  5850 800  800 
U 5E7FCA71
F0 "Wireless Controller" 60
F1 "GNSSDO-Rb-Signal-Generator-v10_SS_5.sch" 60
$EndSheet
$Sheet
S 3500 950  800  800 
U 5E7FCA73
F0 "GNSS" 60
F1 "GNSSDO-Rb-Signal-Generator-v10_SS_6.sch" 60
$EndSheet
$Sheet
S 3500 2200 800  800 
U 5E7FCA75
F0 "USB Isolator" 60
F1 "GNSSDO-Rb-Signal-Generator-v10_SS_7.sch" 60
$EndSheet
$Sheet
S 3500 3400 800  800 
U 5E7FCA77
F0 "I2C Isolator" 60
F1 "GNSSDO-Rb-Signal-Generator-v10_SS_8.sch" 60
$EndSheet
$EndSCHEMATC
