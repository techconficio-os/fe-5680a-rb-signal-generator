EESchema Schematic File Version 4
LIBS:Rb-Multi-Reference-v10-cache
EELAYER 29 0
EELAYER END
$Descr USLegal 14000 8500
encoding utf-8
Sheet 7 9
Title "GNSSDO Rb Signal Generator"
Date "2020-11-23"
Rev "0.2.0"
Comp "TechConficio Inc."
Comment1 ""
Comment2 "License : CC BY 4.0"
Comment3 "Author : Steven Knudsen"
Comment4 "Author : Steven Knudsen"
$EndDescr
$Comp
L Rb-Multi-Reference-v10-rescue:FRAMES_LETTER_L-Rb-Multi-Reference-v10-Rb-Multi-Reference-v10-rescue #FRAME
U 1 1 5E8025F3
P 5950 11250
F 0 "#FRAME" H 5950 11250 50  0001 C CNN
F 1 "~" H 5950 11250 50  0001 C CNN
F 2 "" H 5950 11250 50  0001 C CNN
F 3 "" H 5950 11250 50  0001 C CNN
	1    5950 11250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
