EESchema Schematic File Version 4
LIBS:Rb-Multi-Reference-v10-cache
EELAYER 29 0
EELAYER END
$Descr USLegal 14000 8500
encoding utf-8
Sheet 5 9
Title "GNSSDO Rb Signal Generator"
Date "2020-11-23"
Rev "0.2.0"
Comp "TechConficio Inc."
Comment1 ""
Comment2 "License : CC BY 4.0"
Comment3 "Author : Steven Knudsen"
Comment4 "Author : Steven Knudsen"
$EndDescr
$Comp
L Rb-Multi-Reference-v10-rescue:SPARKFUN-PASSIVES_CAP0603-CAP-Rb-Multi-Reference-v10-Rb-Multi-Reference-v10-rescue C33
U 1 1 5E80126A
P 3300 2850
F 0 "C33" H 3360 2965 70  0000 L BNN
F 1 "0.1 uF" H 3360 2765 70  0000 L BNN
F 2 "SPARKFUN-PASSIVES_0603-CAP" H 3290 2640 65  0001 L TNN
F 3 "" H 3300 2850 50  0001 C CNN
	1    3300 2850
	0    -1   -1   0   
$EndComp
$Comp
L Rb-Multi-Reference-v10-rescue:SPARKFUN-PASSIVES_CAP0603-CAP-Rb-Multi-Reference-v10-Rb-Multi-Reference-v10-rescue C34
U 1 1 5E801332
P 4000 2750
F 0 "C34" H 4059 2865 70  0000 L BNN
F 1 "0.1 uF" H 4060 2665 70  0000 L BNN
F 2 "SPARKFUN-PASSIVES_0603-CAP" H 3990 2540 65  0001 L TNN
F 3 "" H 4000 2750 50  0001 C CNN
	1    4000 2750
	-1   0    0    1   
$EndComp
$Comp
L Rb-Multi-Reference-v10-rescue:FRAMES_LETTER_L-Rb-Multi-Reference-v10-Rb-Multi-Reference-v10-rescue #FRAME
U 1 1 5E801396
P 5950 11250
F 0 "#FRAME" H 5950 11250 50  0001 C CNN
F 1 "~" H 5950 11250 50  0001 C CNN
F 2 "" H 5950 11250 50  0001 C CNN
F 3 "" H 5950 11250 50  0001 C CNN
	1    5950 11250
	1    0    0    -1  
$EndComp
$Comp
L Rb-Multi-Reference-v10-rescue:KSK-CONNECTORS_MCX_JACK_THRU-Rb-Multi-Reference-v10-Rb-Multi-Reference-v10-rescue J6
U 1 1 5E8014C2
P 9200 4350
F 0 "J6" H 9100 4570 70  0000 L BNN
F 1 "MCX_JACK_THRU" H 9100 4470 70  0000 L BNN
F 2 "KSK-CONNECTORS_MCX_THRU_JACK" H 9190 4140 65  0001 L TNN
F 3 "" H 9200 4350 50  0001 C CNN
	1    9200 4350
	-1   0    0    1   
$EndComp
$EndSCHEMATC
